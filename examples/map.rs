use streem::IntoStreamer;

fn main() {
    futures_executor::block_on(async {
        let input_stream = std::pin::pin!(streem::from_fn(|yielder| async move {
            for i in 0..10 {
                yielder.yield_(i).await;
            }
        }));

        let map_stream = std::pin::pin!(streem::from_fn(|yielder| async move {
            let mut streamer = input_stream.into_streamer();

            while let Some(item) = streamer.next().await {
                yielder.yield_(item * 2).await;
            }
        }));

        let mut streamer = map_stream.into_streamer();

        while let Some(item) = streamer.next().await {
            println!("Yielded {item}");
        }
    })
}
